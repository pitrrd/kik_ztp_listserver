﻿#include <iostream>
#include <SFML/Network.hpp>
#include <conio.h>
#include <thread>

#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-network-d.lib")

#define SERVER_PORT 5000
#define SERVER_IP "83.28.171.9"

struct server_info {
	sf::IpAddress server_ip;
	sf::Uint16 port;
	std::string name;
};

int main() {
	std::vector<server_info> servers;    //lista serwerow/pokojow
	std::vector<sf::TcpSocket*> clients; //lista polaczonych graczy
	sf::TcpListener listener;

	server_info tmp_server;
	sf::Packet sizePacket;
	sf::Packet serversPacket;
	sf::Packet request;
	std::string command;
	sf::Packet newServer;

	listener.listen(SERVER_PORT);
	listener.setBlocking(false);

	std::cout << "Server is listening to port " << SERVER_PORT << ", waiting for a message... " << std::endl;
	sf::TcpSocket *client = new sf::TcpSocket();

	while (1) {
		while (listener.accept(*client) == sf::Socket::Done) {		// oczekujemy na polaczenie nowych graczy
			clients.push_back(client);
			std::cout << "new client" << std::endl;
		}

		for (auto client : clients) {
			client->setBlocking(false);
			if (client->receive(request) == sf::Socket::Done) {		//czekamy na pakiet z komendą od klienta
				request >> command;					

				if (command == "get") {	//komenda "get", wysylamy liste serwerow do klienta
					sizePacket.clear();
					serversPacket.clear();
					sizePacket << (sf::Uint16)servers.size();
					std::cout << servers.size();
					for (auto i : servers) {
						serversPacket << i.name << i.port << i.server_ip.toInteger();
						std::cout << i.name << i.port << std::endl;
					}
					client->send(sizePacket);
					client->send(serversPacket);
				}
				else if (command == "newserver") { //komenda "newserver", czekamy na kolejny pakiet z opisem pokoju i dodajemy go do listy
					serversPacket.clear();
					if (client->receive(serversPacket) == sf::Socket::Done) {	
						int ip_tmp;
						serversPacket >> tmp_server.name >> tmp_server.port >> ip_tmp; //structure of packet -> name, port, ip, name, port, ip...
						tmp_server.server_ip = sf::IpAddress(ip_tmp);
						servers.push_back(tmp_server);
						std::cout << "New server: " << tmp_server.name << " " << tmp_server.port << std::endl;
					}
				}
			}
		}
	}

	return 0;
}